﻿using TodoApi.Models;


namespace Web.Api.IntegrationTests
{
    public static class SeedData
    {
        public static void PopulateTestData(TodoContext dbContext)
        {
            dbContext.TodoItems.Add(new TodoItem(1, "do homework", false));
            dbContext.SaveChanges();
        }
    }
}
