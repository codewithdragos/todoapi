#!/bin/bash
# decompress the artifact
tar -xzf artifact.tar.gz
# restart the app service
sudo systemctl restart dotnet-rest.service
