﻿using System;
namespace TodoApi.Models
{
    public class TodoItem
    {
        public TodoItem(long id, String Name, bool isComplete)
        {
            this.Id = id;
            this.Name = Name;
            this.IsComplete = isComplete;
        }
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsComplete { get; set; }
    }
}
