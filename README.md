## Deployment

The app deploys on AWS Linux following the setup described in the [official documentation](https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/linux-nginx?view=aspnetcore-5.0)

## Setup on a fresh instance

#### 1. Install system dependencies
- nginx(proxy server): `sudo amazon-linux-extras install nginx1`
- dotnet SDK(to run the app): `sudo yum install dotnet-sdk-5.0`
- tar(to decompress the artifact): `sudo yum install tar`

Utils:
- check nginx version: `nginx -v`
- check dotnet version: `dotnet -version`


#### 1.1 Add a Nginx config:

Add a new config to the default nginx server:

```bash
sudo nano /etc/nginx/default.d/dotnet-rest.conf
```

Add this to the new file so nginx will forward request to the .net app(running as a service):

```
    location / {
        proxy_pass         http://127.0.0.1:5000;
        proxy_http_version 1.1;
        proxy_set_header   Upgrade $http_upgrade;
        proxy_set_header   Connection keep-alive;
        proxy_set_header   Host $host;
        proxy_cache_bypass $http_upgrade;
        proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header   X-Forwarded-Proto $scheme;
    }

```

#### 2. Create a service file:

`sudo nano /etc/systemd/system/dotnet-rest.service`

Add the following into it:

```bash
[Unit]
Description=Example .NET Web API App running on AWS Linux

[Service]
WorkingDirectory=/home/ec2-user/publish
ExecStart=/usr/bin/dotnet /home/ec2-user/publish/TodoApi.dll
Restart=always
# Restart service after 10 seconds if the dotnet service crashes:
RestartSec=10
KillSignal=SIGINT
SyslogIdentifier=dotnet-example
User=ec2-user
Environment=ASPNETCORE_ENVIRONMENT=Production
Environment=DOTNET_PRINT_TELEMETRY_MESSAGE=false

[Install]
WantedBy=multi-user.target
```

#### 3. Enable the service:

`sudo systemctl enable dotnet-rest.service`

Start the service:
`sudo systemctl start dotnet-rest.service`

You can view service logs by running:

`sudo journalctl -fu dotnet-rest.service`

#### 4. Re-Start the service:

sudo systemctl restart dotnet-rest.service

### Debugging 

You can inspect the running ports by running: 

`sudo lsof -i -P -n | grep LISTEN`

You can reload the linux services by running:

`sudo systemctl daemon-reload`